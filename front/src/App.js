import 'bootstrap/dist/css/bootstrap.min.css'
import 'font-awesome/css/font-awesome.min.css'
import './App.css'
import React from 'react'
import { BrowserRouter } from 'react-router-dom'

import Routes from './Routes/Routes'

//* Header não foi importado devido ao fato do 'Main' já trazer consigo o header
import Logo from './components/templates/Logo/Logo'
import Nav from './components/templates/Nav/Nav'
import Footer from './components/templates/Footer/Footer'
// import Main from './components/templates/Main/Main' //*
// import Home from './components/home/Home'

export default props =>
    <BrowserRouter>
        <div className="app">
            <Logo />
            <Nav />
            <Routes />
            <Footer />
        </div>
    </BrowserRouter>