import React from 'react'
import { Link } from 'react-router-dom'

import './Nav.css'

export default props => 
    <aside className="menu-area">
        {/*Refatorar para component*/}
        <nav className="menu">
            <Link to="/">
                <i className="fa fa-home"></i> Ínicio
            </Link>
            <Link to="/users">
                <i className="fa fa-users"></i> Usuário
            </Link>
        </nav>
    </aside>