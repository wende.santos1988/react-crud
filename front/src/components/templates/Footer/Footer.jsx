import './Footer.css'
import React from 'react'

export default props => 
    <footer className="footer">
        <span>
        <i className="fa fa-keyboard-o text-success"></i> Desenvolvido com por
            <strong> Wendel Santos</strong>
        </span>
    </footer>