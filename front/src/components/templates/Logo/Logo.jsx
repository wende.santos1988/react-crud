import React from 'react'
import { Link } from 'react-router-dom'

import './Logo.css'
// import logo from '../../../assets/images/logo.png'
// import logoPD from '../../../assets/images/logoPD.png'
import logoPDdark from '../../../assets/images/logoPDdark.png'



export default props => 
    <aside className="logo">
        <Link to="/" className="log">
            <img src={logoPDdark} alt="logo" />
        </Link>
    </aside>
