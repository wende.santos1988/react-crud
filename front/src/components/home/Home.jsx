import React from 'react'

import Main from '../templates/Main/Main'

export default props =>
    <Main icon="home" title="Ínicio"
        subtitle="Projeto CRUD React.">
        <div className="display-4">
            Bem Vindo!
        </div>
        <hr />
        <p className="mb-0">Sistema para cadastro usando React no Frontend</p>
    </Main>