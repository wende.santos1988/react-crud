# Tutorial 

Instruções simples para rodar o projeto.

## Instalação

Entre nas pastas de ambos os projetos instale as dependencias com o comando:

```bash
npm install ou yarn install
```
 
Utilização: apos instalar as dependencias você precisa iniciar os projetos em seus respectivos diretorios com o comando:

```bash
npm start ou yarn start
```

## License
[MIT](https://choosealicense.com/licenses/mit/)